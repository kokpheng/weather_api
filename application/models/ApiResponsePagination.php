<?php
/**
 * Created by PhpStorm.
 * User: kokpheng
 * Date: 12/30/17
 * Time: 10:29 AM
 */

/**
 * @SWG\Definition(type="object")
 */
class ApiResponsePagination extends ApiResponse
{
    /**
     * @SWG\Property
     * @var object
     */
    public $pagination;
}