<?php

/**
 * @SWG\Tag(
 *   name="city",
 *   description="Everything about your Cities",
 *   @SWG\ExternalDocumentation(
 *     description="Find out more",
 *     url="http://swagger.io"
 *   )
 * )
 * @SWG\Tag(
 *   name="forecast",
 *   description="Forecast of the city"
 * )
 *
 * @SWG\Tag(
 *   name="file",
 *   description="file upload"
 * )
 */
